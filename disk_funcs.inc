num_sectors db 0
target_address dw 0
print_disk_error:
bios_print DISK_ERROR
mov ah,0x86
mov cx,0x1
mov dx,0x86a0
int 0x15 ; sleep 1 second
read_disk:
xor ah,ah
int 0x13
mov ah,0x02
mov al,[num_sectors]
xor dl,dl ; drive 0
xor dh,dh ; head 0
xor ch,ch ; cylinder 0
mov cl,2 ; start from sector 2 (1 is our boot sector)
xor bx,bx
mov es,bx
mov bx, [target_address] ; copy data to end of fisrst sector in memory

int 0x13
jc short print_disk_error

cmp al,[num_sectors]
jne short print_disk_error

cmp ah,0
jne short print_disk_error

ret

DISK_ERROR:
  db "Disk read Error!",0xa,0xd,0
