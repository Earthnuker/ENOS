org 0x7e00

jmp short ldr_entry_point

include 'gdt.inc'

vmem equ 0xb8000

GDT_MSG:
  db "Loading GDT...",0xa,0xd,0
PARTY_MODE:
  db "Entering Protected Mode and engaging Rave Mode!",0xa,0xd,0

ldr_entry_point:

bios_print GDT_MSG
lgdt [gdt_descriptor]

bios_print PARTY_MODE

mov ax,0
mov ds,ax
mov es,ax
mov fs,ax
mov gs,ax
mov ss,ax

mov eax,cr0
or eax,0x1
mov cr0,eax
jmp CODE_SEG:pm_entry_point

use32
pm_entry_point:
  mov ax,DATA_SEG
  mov ds,ax
  mov es,ax
  mov ss,ax
  mov ebp, 0x90000
  mov esp , ebp
  call protected_mode
  cli
  hlt
  jmp $


protected_mode:
  mov bl,0
  xor eax,eax
  .print_loop:
  inc eax
  mov byte [vmem+eax],cl ; attr
  inc eax
  cmp eax,(80*25*2)
  jl .print_loop
  inc cl
  mov eax,0
  jmp .print_loop
  ret
