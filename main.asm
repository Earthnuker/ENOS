use16
format binary as "img"
org 0x7c00  ; image is loaded at 0x7c00 in memory
jmp short start
include 'string_funcs.inc'
include 'disk_funcs.inc'
start:
mov sp,0x7c00
bios_print STAGE2_MSG
mov [num_sectors],0x8
mov [target_address],0x7e00
call read_disk

jmp [target_address]

STAGE2_MSG:
  db "Loading and Executing Stage 2 Loader...",0xa,0xd,0

padding: times 446 - ($-$$) db 0
partitiontable: times 64 db 0xff
signature: dw 0xaa55 ; boot signature

include 'stage2.asm'

times 512 - ($-$$) db 0
