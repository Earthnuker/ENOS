default: all

all: main.img

test: main.img
	@echo executing bochs
	bochs -q

debug: main.img
	@echo executing bochsdbg
	bochsdbg -q 'display_library: win32, options="gui_debug"'

%.img: %.asm
	@echo assembling $< into $@
	fasm $< $@

clean:
	rm -rf *.img *.o bx_enh_dbg.ini
