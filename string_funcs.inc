macro bios_print string {
  mov si,string
  call print_string
}

print_string:
  ; print string pointed to by si
  pushad
  .next_char:
  lodsb            ; load next char into al
  or al,al        ; check if we reached the end of the string
  jz .return       ; if we did return
  mov ah,0x0e
  int 0x10         ; print character
  jmp short .next_char   ; loop back
  .return:
  popad
  ret
