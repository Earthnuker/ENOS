To assemble run

`make`

To test run 

`make test`

To run under Bochs Debugger run

`make debug`

(you'll need to change the Makefile, if you're not on windows,
at the moment it uses the win32 gui frontend for the debugger)