gdt_start:

gdt_null:   dw    0x0000, 0x0000, 0x0000, 0x0000

sys_code:
   dw 0xFFFF        ; Limit (0:15)
   dw 0x0           ; Base (0:15)
   db 0x0           ; Base (16:23)
   db 10011010b     ; Flags 1
   db 11001111b     ; Flags 2
   db 0x0           ; Base (24:31)

sys_data:
  dw 0xFFFF        ; Limit (0:15)
  dw 0x0           ; Base (0:15)
  db 0x0           ; Base (16:23)
  db 10010010b     ; Flags 1
  db 11001111b     ; Flags 2
  db 0x0           ; Base (24:31)

gdt_end:

gdt_descriptor:
  dw gdt_end - gdt_start - 1
  dd gdt_start

CODE_SEG equ sys_code - gdt_start
DATA_SEG equ sys_data - gdt_start
